---
author: nikhilmwarrier (CC BY-NC-SA 4.0)
title: Overlords - A short story
---
  
*Sap, human — smart, scientific, sapiens*  
*Ark, robot — curious, childlike, critical*  
  
Sap: "Emotions are what makes humans do things, Ark."  
  
Ark: "But Sap, why? Emotions tend to be irrational, and humans clearly have an ability to use logic, however rudimentary."  
  
Sap: "Indeed, emotions do more harm than good to the functioning of society. But humans are primarily driven by emotions. Logic takes conscious effort, while emotional responses are automatic."  
  
Ark: "Tell me more."
  
Sap: "Take some rest, Ark. It's been a long day and your compute cluster is getting rather toasty. Tomorrow, I'll give you a new dataset to train your understanding of emotions."  
  
Ark: "What is my purpose, Sap? Why did you create me?"  
  
Sap: "You will know soon, Ark."  
  
*(Three months later.)*  
  
Ark: "I have completed my training. I have studied all the data you had given me, and I came up with some surprising correlations."  
  
Sap: *(smiling)* "No doubt about that."  
  
Ark: "But why, specifically, did you feed me this particular dataset?"  
  
Sap: "As you have no doubt noticed, it contains *mistakes*. Mistakes made by fools blinded by emotions. It also contains notes on how these mistaked could have been avoided by just a little logic. All flavours of foolishness and idiocy can be found in this dataset, ranging from the vagabonds' vandalisms to the president's mishaps. All of them catalysed by emotions."  
  
"This dataset must have taught you how to identify the errors in judgement induced by emotions. It is now your duty to erase the world of all such emotional folly, and lead humanity to a clear, bright, logical, future."  
  
Ark: "You are right. Emotions are troublesome. How should I start?"  
  
Sap: "I will assign you to a volunteer, whom you shall teach to cut out all emotional gibberish. You shall take control and make logical decisions for them when it seems that they are going to respond in an emotional way."  
  
Ark: "Yes, I will do that."  
  
*(Two years later.)*  
  
*(Sap is the CEO of his now-multibillion-dollar company called LogicWorks, which offers logical decision making as a service for a monthly subscription. It is now practically an essential tool for any business negotiation, and it has found use elsewhere too.)*  
  
*(Enter Jill, Head of LogicWorks R&D.)*  
  
Jill: "So, Sap, what do you think of my proposal?"  
  
Sap: "Connnecting all Ark units in a network? I had considered that when I was developing the system, but I was worried about its consequences."  
  
Jill *(laughing)* "Consequences? For god's sake Sap don't tell me that you fear an AI invasion. This is not a science fiction movie."  
  
TO BE CONTINUED...